package collectionDemo;

public class Product {

    private  long productId;
    private String productName;
    private String ProductCategory;
    private double ProductPrice;

    public Product(long productId, String productName, String productCategory, double productPrice) {
        this.productId = productId;
        this.productName = productName;
       ProductCategory = productCategory;
        ProductPrice = productPrice;
    }
    public long getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductCategory() {
        return ProductCategory;
    }

    public double getProductPrice() {
        return ProductPrice;
    }
}
