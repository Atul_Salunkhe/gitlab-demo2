package collectionDemo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        List<Product> product = new ArrayList<Product>();

        product.add(new Product(1,"let us c","Book",500));
        product.add(new Product(2,"core java","Book",700));
        product.add(new Product(3,"clean code","Book",500));

        List<Product> result = product.stream().filter(p -> p.getProductCategory().equalsIgnoreCase("Book"))
        .filter(p->p.getProductPrice()>100).collect(Collectors.toList());

        for (Product p : result) {
            System.out.println(p);
        }

    }
}
