package com.example.awtSample;

import org.apache.commons.lang.StringUtils;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        String s1 = new String("  Java Language ");
        String s2 = new String();
        String s3 = new String("james gosling ");

        //isEmpty();
        System.out.println("isEmpty" + StringUtils.isEmpty(s1));      //false
        System.out.println("isEmpty" + StringUtils.isEmpty(s2));
        if (StringUtils.isEmpty(s2)) {
            System.out.println("String is empty.");
        }
        //isNotEmpty();
        System.out.println("isNotEmpty" + StringUtils.isNotEmpty(s1));   //

        // isBlank();
        System.out.println("String is Blank=" + StringUtils.isBlank(s1));      // false

        //isNotBlank();
        System.out.println("String is Not empty=" + StringUtils.isNotBlank(s1));   // true

        //trim()
        String sTrim = StringUtils.trim(s1);
        System.out.println("sTrim()" + sTrim);
        //System.out.println("trim() ="+StringUtils.trim(s1));

        //strip()
        String sStrip = StringUtils.trim(s1);
        System.out.println("strip()" + sStrip);
        //System.out.println("strip() ="+StringUtils.strip(s1));


<<<<<<< HEAD
            //equals();
            if(StringUtils.equals(s1,s3)){
                    System.out.println("String is equal");
            } else{
                    System.out.println("Stirng is not equal");
            }
            String s4 = new String("  Java Language ");
            System.out.println("String Equals(s1,s4)="+StringUtils.equals(s1,s4));
=======


        //compare();
        System.out.println("String  Compare(s1,s2)=" + StringUtils.compare(s1, s3));
        System.out.println("String  compare(s1,s4)=" + StringUtils.compare(s1, s4));

        String s5 = new String("Software");
        //startwith
        System.out.println("StartsWith = " + StringUtils.startsWith(s5, "j"));

        //endsWith
        System.out.println("endsWith = " + StringUtils.endsWith(s5, "a"));

        //IndexOf()
        System.out.println("IndexOf=" + StringUtils.indexOf(s5, "a"));

        // isNumeric();
        System.out.println("String is Numeric= " + StringUtils.isNumeric(s1));

        //join();
        List<String> names = Arrays.asList("java", "javascipt", "c");
        System.out.println("String Joined with comma =" + StringUtils.join(names, " "));
        System.out.println("String Joined with comma =" + StringUtils.join(names, ","));

        //without SstringUtils()
        System.out.println("without StringUtils join = " + String.join(",", names));

        //abbreviate();
        String abbreviated = StringUtils.abbreviate(s1, 12);
        System.out.println("abbreviate = " + abbreviated);


    }
}
